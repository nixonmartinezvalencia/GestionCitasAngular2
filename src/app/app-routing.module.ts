import { NgModule} from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { InicioComponent } from './inicio/inicio/inicio.component';




const routes: Routes = [
 
  
  { path: 'inicio', component: InicioComponent },

  { 
    path: 'citas' ,
    loadChildren: () => import('./Citas/citas.module').then(mod => mod.CitasModule)

  },
  { 
    path: 'medicos',
    loadChildren: () => import('./Medicos/medicos.module').then(mod => mod.MedicosModule)
  },

  { 
    path: 'pacientes' ,
    loadChildren: () => import('./Pacientes/pacientes.module').then(mod => mod.PacientesModule)
  },

  { 
    path: 'users' ,
   loadChildren: () => import('./users/users.module').then(mod => mod.UsersModule)
  
  },

  { path: '**', redirectTo: 'inicio', pathMatch: 'full' }
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes
    /*,{

    enableTracing:true,
    preloadingStrategy:PreloadAllModules
  }*/
    
    )],
  exports: [RouterModule]
})
export class AppRoutingModule { }


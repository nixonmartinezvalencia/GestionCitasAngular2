import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { interceptorProvider } from './inicio/Security/_interceptors/interceptor.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InicioModule } from './inicio/inicio.module';
import { GaleriaComponent } from './inicio/galeria/galeria.component';
import { EncabezadoComponent } from './inicio/encabezado/encabezado.component';
import { RangoMaximoDirective } from './inicio/Validations/rango-maximo.directive';






@NgModule({
  declarations: [
    AppComponent,
    GaleriaComponent,
    EncabezadoComponent,RangoMaximoDirective
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
   
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    InicioModule
  ],
  providers: [interceptorProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cita } from 'src/app/Citas/model/Cita';
import { CitaServiceService } from 'src/app/Citas/service/cita-service.service';

@Component({
  selector: 'app-listar-citas',
  templateUrl: './listar-citas.component.html',

})
export class ListarCitasComponent implements OnInit {

  citas :Cita[];
  constructor(private  service:CitaServiceService, private router:Router) { }

  ngOnInit(){
    this.service.getCitas().subscribe(data=>{

      this.citas=data;
    })
  }

  insertarCita(){
    this.router.navigate(["citas","insertarCita"]);
  }

}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders,HttpRequest,HttpResponse} from '@angular/common/http';
import { Cita } from '../model/Cita';
import { Response } from '../model/Response';
import { environment } from 'src/environments/environment';

const header = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
  providedIn: 'root'
})
export class CitaServiceService {



  private url :string;

  constructor(private http:HttpClient) {

    this.url = `${environment.Urllocalhost}/citas`;
   }

  getCitas(){
    return this.http.get<Cita[]>(this.url,header);  
  }

  createCita(cita:Cita){
    return this.http.post<Response>(this.url,cita,header); 
  }
}

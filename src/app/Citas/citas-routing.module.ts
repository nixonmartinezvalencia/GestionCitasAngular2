import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarCitasComponent } from './listar-citas/listar-citas.component';
import { InsertarCitaComponent } from './insertar-cita/insertar-cita.component';


const routes: Routes = [

    { 
      path: '' ,
      children:[

        {

          path:'listarCitas',
          component :ListarCitasComponent,
        
        },
        {

          path:'insertarCita',
          component :InsertarCitaComponent,
        
        }
      ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CitasRoutingModule { }

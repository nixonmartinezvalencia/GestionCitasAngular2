import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CitasRoutingModule } from './citas-routing.module';
import { ListarCitasComponent } from './listar-citas/listar-citas.component';
import { InsertarCitaComponent } from './insertar-cita/insertar-cita.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    ListarCitasComponent,
    InsertarCitaComponent
    
  ],
  imports: [
    CommonModule,
    CitasRoutingModule,
    FormsModule
  ]
})
export class CitasModule { }

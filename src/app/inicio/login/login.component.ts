import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/users/service/user.service';

import {NgxSpinnerService} from 'ngx-spinner'
import { AuthService } from 'src/app/inicio/Security/_auth/auth.service';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';
import { User } from 'src/app/users/model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  form: any = {};
  user: User;
  isLogged = false;
  isLoginFail = false;
  roles: string[] = [];
  errorMsg = '';
  rutaLogo;

  constructor(private router: Router,
    private authService: AuthService,
    private tokenService: TokenService,
    private spinnerService : NgxSpinnerService,
    private userService: UserService) { }

  ngOnInit() {
    this.rutaLogo=environment.rutalogo;
    this.spinner();
    if (this.tokenService.getToken()) {
      this.isLogged = true;
      this.isLoginFail = false;
      this.roles = this.tokenService.getAuthorities();
    }

  
  }

  spinner(){
    this.spinnerService.show();
    setTimeout(() => {
      this.spinnerService.hide();
    }, 1000);
  }

  onLogin(): void {
    this.spinnerService.show();
    this.user = new User(this.form.domain, this.form.password);     
    this.authService.login(this.user).subscribe(data => {
      this.tokenService.setToken(data.token);
      this.tokenService.setUserName(data.username);
      this.tokenService.setAuthorities(data.authorities);     
     this.roles = this.tokenService.getAuthorities();
      this.userService.detailByName(this.tokenService.getUserName())
          .subscribe(data =>{            
            if(data.enable){
              this.spinnerService.hide();
              this.isLogged = true;
              this.isLoginFail = false;              
              window.location.reload();
            } else{
              this.spinnerService.hide();
              this.isLogged = false;
              this.isLoginFail = true;
              this.errorMsg = "El usuario se encuentra inactivo";
            }
      }, (err:any)=>{
        this.spinnerService.hide();
      });     
    },
      (err: any) => {
        this.spinnerService.hide();
        this.isLogged = false;
        this.isLoginFail = true;
        this.errorMsg = err.error.message;
      }
    );
  }

  newAccess(username){
    this.userService.detailByName(username)
    .subscribe(data => {      
      data.lastAccess = new Date();
      this.userService.edit(data, data.id)
      .subscribe(dat => {
        
      },
        (err: any) => {
          console.log(err);
        }
      );
    },
      (err: any) => {
        console.log(err);
      }
    );
  }


}

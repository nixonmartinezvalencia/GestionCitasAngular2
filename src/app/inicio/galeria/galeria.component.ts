import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenService } from '../Security/_auth/token.service';
import { UserService } from '../../users/service/user.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  
})
export class GaleriaComponent implements OnInit {


  isLogin = false;
  permissions: string[];
  manage: boolean = false;
  autor : boolean = false;
  editor : boolean = false;
  currentUser: string;
  newMessage: number = 0;

  rutaLogo;

  constructor(private router:Router,  private tokenService: TokenService, private userService: UserService) {


   }

  ngOnInit(): void {


    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();
    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;
      this.permissions = [];
      this.getCurrentUser(this.currentUser);
      this.permissions = this.tokenService.getAuthorities();
      for (let permission of this.permissions) {
        if (permission.indexOf("CRUD Usuarios") === 0) {
          this.manage = true;
        }
       
        if (permission.indexOf("Autor") === 0) {
          this.autor = true;  
        }
        if (permission.indexOf("Editor") === 0) {
          this.editor = true;  
        }
       
      }
    }else{
     
      this.logOut();
     
    }


  }

  listarMedicos(){
    this.router.navigate(["medicos","listarMedicos"]);
  }
  listarPacientes(){
    this.router.navigate(["pacientes","listarPacientes"]);
  }
  listarCitas(){
    this.router.navigate(["citas","listarCitas"]);
  }



  
  getCurrentUser(userName: string) {
    this.userService.detailByName(userName)
      .subscribe(data => {
        //this.getNewMessages(data.id);
      },
        (err: any) => {
          console.log(err);
        }
      );
  } 

  logOut(): void {
    this.tokenService.logOut();
    this.isLogin = false;
    this.router.navigate(['inicio']);
  }
  

}

import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html'
})
export class InicioComponent implements OnInit {

  info: any = {};

  constructor(private tokenService: TokenService) { }

  ngOnInit() {
    
    this.info = {
      token: this.tokenService.getToken(),
      email: this.tokenService.getUserName(),
      authorities: this.tokenService.getAuthorities()
    };
  }
}

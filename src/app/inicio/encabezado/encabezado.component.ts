import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/users/service/user.service';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';



@Component({
  selector: 'app-encabezado',
  templateUrl: './encabezado.component.html',
  styleUrls: ['./encabezado.component.css']
})
export class EncabezadoComponent implements OnInit {
  isLogin = false;
  permissions: string[];
  currentUser: string;

  rutaLogo;

  constructor(
    private tokenService: TokenService,
    private userService: UserService,
    private router: Router
  ) { 
  }

  ngOnInit() {
    this.rutaLogo=environment.rutalogo;

    this.currentUser = this.tokenService.getUserName();
    const helper = new JwtHelperService();
    if (!helper.isTokenExpired(this.tokenService.getToken())) {
      this.isLogin = true;
      this.permissions = [];
      this.getCurrentUser(this.currentUser);
      this.permissions = this.tokenService.getAuthorities();
     
    }else{
      this.logOut();
    }
  }

  getCurrentUser(userName: string) {
    this.userService.detailByName(userName)
      .subscribe(data => {
        
      },
        (err: any) => {
          console.log(err);
        }
      );
  }


 

  logOut(): void {
    this.tokenService.logOut();
    this.isLogin = false;
    
    this.router.navigate(['inicio']);
    
  }

}

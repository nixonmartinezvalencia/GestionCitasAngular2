import { TestBed, inject, async } from '@angular/core/testing';
//import {MockBackend} from '@angular/http/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import { MedicoServiceService } from './Medicos/service/medico-service.service';
import { Medico } from './Medicos/model/Medico';




  fdescribe('get data', () => {


    
  let service: MedicoServiceService;
   let  httpTestingController: HttpTestingController;

 beforeEach( () => {
   TestBed.configureTestingModule({
     imports: [HttpClientTestingModule],
     providers: [MedicoServiceService,Medico]
   });

    service = TestBed.get(MedicoServiceService);
    httpTestingController = TestBed.get(HttpTestingController);
 });


 afterEach( () => {
  httpTestingController.verify();

});

    /*it('should get results',
    inject([HttpTestingController, MedicoServiceService], (httpMock: HttpTestingController, serviceMemory: MedicoServiceService,medico:Medico) => {
      const swapiUrl = 'http://localhost:8484/apiCitas/medicos/1024578448';
      
      serviceMemory.getMedicosId("1024578448")
      .subscribe(
        (resultado) => {
            expect(resultado.finAtencion).toEqual("15");
        }
      );
      const req = httpMock.expectOne(swapiUrl);
      expect(req.request.method).toBe('GET');
      //req.flush(mockResponse);
    })
  ); */

  it('rest medico',() => {

    medico:Medico;


  const mockResponse:Medico[] =[ {
    nombre:"sandrita",
    identificacion:"1024578448",
    tipoIdentificacion:"tarjeta identidad",
    apellido:"gaucho",
    numeroTarjetaProfesional:"454",
    tiempoExperiencia:24.0,
    especialidad:"hhh",
    inicioAtencion:"8",
    finAtencion:"156"
   
  }];
    const swapiUrl = 'http://localhost:8484/apiCitas/medicos';
    
    service.getMedicos()
    .subscribe(
      resultado => {
         // expect(resultado).toEqual(mockResponse);
          //expect(resultado.length).toBe(66);
          expect(resultado[0]).toEqual(mockResponse[0]);
          expect(resultado[0].finAtencion).toEqual("156");
      }
    );


    const req = httpTestingController.expectOne(swapiUrl);
    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toEqual('json');
    
    req.flush(mockResponse);
  });


  
  });

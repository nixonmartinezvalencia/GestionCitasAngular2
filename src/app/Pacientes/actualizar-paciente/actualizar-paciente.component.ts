import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Paciente } from 'src/app/Pacientes/model/Paciente';
import { PacienteServiceService } from 'src/app/Pacientes/service/paciente-service.service';

@Component({
  selector: 'app-actualizar-paciente',
  templateUrl: './actualizar-paciente.component.html',
 
})
export class ActualizarPacienteComponent implements OnInit {

  paciente :Paciente = new Paciente();
  constructor(private service:PacienteServiceService, private router:Router) { }

  ngOnInit() {
    
  }


}

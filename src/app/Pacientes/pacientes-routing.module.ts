import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarPacientesComponent } from './listar-pacientes/listar-pacientes.component';
import { InsertarPacienteComponent } from './insertar-paciente/insertar-paciente.component';
import { ActualizarPacienteComponent } from './actualizar-paciente/actualizar-paciente.component';


const routes: Routes = [

  
  { 
    path: '' ,
    children:[

      {

        path:'listarPacientes',
        component :ListarPacientesComponent,
     
      },

      {

        path:'insertarPaciente',
        component :InsertarPacienteComponent,
     
      },
      {

        path:'actualizarPaciente',
        component :ActualizarPacienteComponent,
       
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacientesRoutingModule { }

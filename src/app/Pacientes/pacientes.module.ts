import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PacientesRoutingModule } from './pacientes-routing.module';
import { ListarPacientesComponent } from './listar-pacientes/listar-pacientes.component';
import { InsertarPacienteComponent } from './insertar-paciente/insertar-paciente.component';
import { ActualizarPacienteComponent } from './actualizar-paciente/actualizar-paciente.component';
import { FormularioPacienteComponent } from './formulario-paciente/formulario-paciente.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ListarPacientesComponent,
    InsertarPacienteComponent,
    ActualizarPacienteComponent,
    FormularioPacienteComponent
  ],
  imports: [
    CommonModule,
    PacientesRoutingModule,
    FormsModule   

  ]
})
export class PacientesModule { }

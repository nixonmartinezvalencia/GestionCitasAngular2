import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Paciente } from '../model/Paciente';
import { environment } from 'src/environments/environment';

const header = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
  providedIn: 'root'
})
export class PacienteServiceService {

  private url :string;

  constructor(private http:HttpClient) { 

    
    this.url = `${environment.Urllocalhost}/pacientes`;
  }

  getPacientes(){

    return this.http.get<Paciente[]>(this.url,header);
  }


  createPaciente(paciente:Paciente){

    return this.http.post<Paciente>(this.url,paciente,header);
  }

  getPacientesId(id:string){
    return this.http.get<Paciente>(`${this.url}/${id}`,header);
  }

  uptdatePaciente(paciente:Paciente){

    return this.http.put<Paciente>(this.url+"/"+paciente.identificacion,paciente,header);
  }

  deletePaciente(paciente:Paciente){
    return this.http.delete<Paciente>(this.url+"/"+paciente.identificacion,header);
  }

}

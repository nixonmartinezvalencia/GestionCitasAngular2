import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListarMedicosComponent } from './listar-medicos/listar-medicos.component';
import { InsertarMedicoComponent } from './insertar-medico/insertar-medico.component';
import { ActualizarMedicoComponent } from './actualizar-medico/actualizar-medico.component';


const routes: Routes = [


  
  { 
    path: '' ,  
    children:[

      {

        path:'listarMedicos',
        component :ListarMedicosComponent,
      },

      {

        path:'insertarMedico',
        component :InsertarMedicoComponent,
      },
      {

        path:'actualizarMedico',
        component :ActualizarMedicoComponent,

      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MedicosRoutingModule { }

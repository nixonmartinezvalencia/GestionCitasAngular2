import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MedicosRoutingModule } from './medicos-routing.module';
import { ListarMedicosComponent } from './listar-medicos/listar-medicos.component';
import { InsertarMedicoComponent } from './insertar-medico/insertar-medico.component';
import { FormularioMedicoComponent } from './formulario-medico/formulario-medico.component';
import { FormsModule } from '@angular/forms';
import { ActualizarMedicoComponent } from './actualizar-medico/actualizar-medico.component';


@NgModule({
  declarations: [
    ListarMedicosComponent,
    InsertarMedicoComponent,
    ActualizarMedicoComponent,
    FormularioMedicoComponent
  ],
  imports: [
    CommonModule,
    MedicosRoutingModule,
    FormsModule   
  ]
})
export class MedicosModule { }

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Medico } from '../model/Medico';
import { environment } from 'src/environments/environment';

const header = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
  providedIn: 'root'
})
export class MedicoServiceService {

  
 private url : string;

  constructor(private http:HttpClient) {

    this.url = `${environment.Urllocalhost}/medicos`;
   }

  getMedicos(){

    return this.http.get<any>(this.url,header);
  }


  createMedico(medico:Medico){

    return this.http.post<Medico>(this.url,medico,header);
  }

  getMedicosId(id:string){
    return this.http.get<Medico>(this.url+"/"+id,header);
  }

  updateMedico(medico:Medico){

    return this.http.put<Medico>(`${this.url}/${medico.identificacion}`,medico,header);
  }

  deleteMedico(medico:Medico){
    return this.http.delete<Medico>(this.url+"/"+medico.identificacion,header);
  }




}

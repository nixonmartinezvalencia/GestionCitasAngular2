import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { UserService } from 'src/app/users/service/user.service';

import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from 'src/app/users/model/user';
import { Role } from 'src/app/users/model/roles';

import { TokenService } from 'src/app/inicio/Security/_auth/token.service';

declare var jQuery: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @ViewChild('createModal') createModal: ElementRef;
  @ViewChild('editModal') editModal: ElementRef;
  @ViewChild('deleteModal') deleteModal: ElementRef;

  users: User[] = [];
  roles: Role[] = [];
 
  roleStr: string[];
  pointStr: string[];
 
  userName: string;
  wrongForm = false;
  errorMessage = "";

  // to create
  form: any = {};
  user: User;
  creado = false;
  failPunto = false;
  mensajeFail = '';
  mensajeOK = '';

  // to edit
  oldUser: User = null;
  formEdit: any = {};
  editRole: boolean = false;
  actualizado = false;
  failActualizado = false;
  msjErr = '';
  msjOK = '';
  failInit = false;

  rutaPen;
  rutaDelete;

  constructor(
    private userService: UserService,
    private tokenService: TokenService,
    private router: Router
  ) { }

  ngOnInit() {
    this.rutaPen=environment.rutaPen;
    this.rutaDelete=environment.rutaDelete;
    this.userName = this.tokenService.getUserName();
    const helper = new JwtHelperService();
    if (!helper.isTokenExpired(this.tokenService.getToken())) {  
   /*   this.loadRoles();
      this.loadPoints();*/
      this.loadUsers();
    } else {
      this.logOut();
    } 
  }

  loadUsers(): void {
    this.userService.list(this.userName).subscribe(data => {
      this.users = data;
    },
      (err: any) => {
        console.log(err);
      }
    );
  }
/*
  loadRoles(): void {
    this.roleService.list(this.userName).subscribe(data => {
      for (let dat of data) {
        if (dat.enable) {
          this.roles.push(dat);
        }
      }
    },
      (err: any) => {
        console.log(err);
      }
    );
  }

  loadPoints(): void {
    this.pointService.list().subscribe(data => {
      for (let dat of data) {
        if (dat.enable) {
          this.points.push(dat);
        }
      }
    },
      (err: any) => {
        console.log(err);
      }
    );
  }
*/
  onSetForm(){
    this.wrongForm=false;
  }

  onDelete(id: string): void {
    const helper = new JwtHelperService();
    if (!helper.isTokenExpired(this.tokenService.getToken())) {  
      this.userService.changeState(id).subscribe(data => {      
        this.loadUsers();
      });
      jQuery(this.deleteModal.nativeElement).modal('hide');
    } else {
      this.logOut();
    }
  }

  onCreate(): void {
    const helper = new JwtHelperService();
    if (!helper.isTokenExpired(this.tokenService.getToken())) {  
      let newUser: any;
      newUser = this.form;
      newUser.username = this.form.username;// + '@quileia3padirectory.onmicrosoft.com';
      newUser.roles = [this.form.roles];
      newUser.enable = true;
      newUser.lastAccess = new Date();
      if((this.form.roles!=[undefined])&&(this.form.name!="")&&(this.form.name!=null)){
        /*this.pointService.detail(this.form.points).subscribe(data => {
          newUser.pointDTO = data;
          this.userService.create(newUser).subscribe(data => {
            this.loadUsers();
            this.mensajeOK = data.mensaje;
            this.creado = true;
            this.failPunto = false;
          },
            (err: any) => {
              this.mensajeFail = err.error.mensaje;
              this.creado = false;
              this.failPunto = true;
            }
          );
        });
        this.form = {};
        jQuery(this.createModal.nativeElement).modal('hide');
        */
      } else {
        this.wrongForm = true;
        this.errorMessage = "datos ingresados inválidos";
      }
    } else {
      this.logOut();
    }    
  }

  currentUser(id: string) {
    this.onSetForm();
    this.userService.detail(id).subscribe(data => {
      this.formEdit = data;
     
    },
      err => {
        console.log(err);
      }
    );
  }

  onUpdate(): void {
    const helper = new JwtHelperService();
    if (!helper.isTokenExpired(this.tokenService.getToken())) {  
      let newUser: User;
      delete this.formEdit.password;
      newUser = this.formEdit;
      if (this.editRole) {
        newUser.rol = this.formEdit.roles;
      } 
      
      newUser.username = this.form.username;
      newUser.lastAccess = new Date();
      if((this.formEdit.name!="")&&(this.formEdit.name!=null)){
        /*
        this.pointService.detail(this.formEdit.points).subscribe(data => {
          newUser.pointDTO = data;
          this.userService.edit(newUser, newUser.id).subscribe(data => {
            this.loadUsers();
            this.actualizado = true;
            this.failActualizado = false;
            this.msjOK = data.mensaje;
          },
            (err: any) => {
              this.actualizado = false;
              this.failActualizado = true;
              this.msjErr = err.error.mensaje;
            });
        });
        this.formEdit = {};
        this.editRole = false;
        jQuery(this.editModal.nativeElement).modal('hide');
      } else{
        this.wrongForm = true;
        this.errorMessage = "datos ingresados inválidos";
        */
      }
    } else {
      this.logOut();
    }    
  }

  isEditRole() {
    this.editRole = true;
  }

  logOut(): void {
    this.tokenService.logOut();
    this.router.navigate(['inicio']);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/users/model/user';
import { TokenService } from 'src/app/inicio/Security/_auth/token.service';

const token = new TokenService().getToken();
const header = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient,private tokenService: TokenService) { }

  public list(userName: string): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.apiUrl}/user/rol/${userName}`, header);
  }

  public listRevisor(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${environment.apiUrl}/users/rol`, header);
  }


  public getSupervisor(idInv: string): Observable<User> {
    return this.httpClient.get<User>(`${environment.apiUrl}/user/role/${idInv}`, header);
  }

  public detail(id: string): Observable<User> {
    return this.httpClient.get<User>(`${environment.apiUrl}/user/${id}`, header);
  }

  public detailByName(name: string): Observable<User> {
  
    return this.httpClient.get<User>(`${environment.apiUrl}/user/name/${name}`,header);
  }

  public create(user: User): Observable<any> {
    return this.httpClient.post<any>(`${environment.apiUrl}/user`, user, header);
  }

  public edit(user: User, id: string): Observable<any> {
    return this.httpClient.put<any>(`${environment.apiUrl}/user/${id}`, user, header);
  }

  public delete(id: string): Observable<any> {
    return this.httpClient.delete<any>(`${environment.apiUrl}/user/${id}`, header);
  }

  public changeState(id: string): Observable<any> {
    return this.httpClient.put<any>(`${environment.apiUrl}/user/state/${id}`, header);
  }

}


import { Role } from './roles';

export class User {
    id?: string="";
    name: string="";
    lastName: string="";
    identification: string="";
    username: string="";    
    password: string="";
    enable: boolean=true;
    lastAccess: Date=new Date();
    rol: Role=  new Role();

    constructor(username: string, password: string) {
        this.username = username;
        this.password = password;
    }
}